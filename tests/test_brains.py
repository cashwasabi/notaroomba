from typing import List
from brains import NotARoomba, MoveCommand, Coordinates


def test_not_a_roomba_clean():
    start = Coordinates()
    move_commands: List[MoveCommand] = [
        MoveCommand(cardinal="E", steps=1),
        MoveCommand(cardinal="S", steps=2),
        MoveCommand(cardinal="E", steps=4),
        MoveCommand(cardinal="N", steps=3),
    ]
    not_a_roomba = NotARoomba(start)
    for move_command in move_commands:
        not_a_roomba.move_to(move_command)
    assert len(not_a_roomba.get_cleaned_places()) == 11


def test_not_a_roomba_with_special_starting_position():
    start = Coordinates(x=-1, y=-2)
    move_commands: List[MoveCommand] = [
        MoveCommand(cardinal="E", steps=1),
        MoveCommand(cardinal="S", steps=2),
        MoveCommand(cardinal="E", steps=4),
        MoveCommand(cardinal="N", steps=3),
    ]
    not_a_roomba = NotARoomba(start)
    for move_command in move_commands:
        not_a_roomba.move_to(move_command)
    assert len(not_a_roomba.get_cleaned_places()) == 11


def test_clean_with_overlaps():
    start = Coordinates()
    move_commands: List[MoveCommand] = [
        MoveCommand(cardinal="E", steps=2),
        MoveCommand(cardinal="W", steps=2),
        MoveCommand(cardinal="E", steps=4),
        MoveCommand(cardinal="N", steps=3),
    ]
    not_a_roomba = NotARoomba(start)
    for move_command in move_commands:
        not_a_roomba.move_to(move_command)
    assert len(not_a_roomba.get_cleaned_places()) == 8
