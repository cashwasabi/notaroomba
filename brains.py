from __future__ import annotations
from typing import List
from dataclasses import dataclass, field


@dataclass
class Coordinates:
    x: int = 0
    y: int = 0

    def __add__(self, coordinates: Coordinates):
        x = self.x + coordinates.x
        y = self.y + coordinates.y
        return Coordinates(x=x, y=y)

    def __hash__(self):
        return hash(str({"x": self.x, "y": self.y}))

    def to_vector(self):
        return [self.x, self.y]

    @classmethod
    def from_vector(cls, vector: List[int]):
        return cls(x=vector[0], y=vector[1])


@dataclass
class MoveCommand:
    cardinal: str
    steps: int

    @staticmethod
    def cardinal_to_vector(cardinal: str) -> List[int]:
        return {"N": [1, 0], "S": [-1, 0], "E": [0, 1], "W": [0, -1]}[cardinal]

    def to_vector(self) -> List[int]:
        return self.cardinal_to_vector(self.cardinal) * steps

    def to_coordinates(self) -> List[Coordinates]:
        return [
            Coordinates.from_vector(self.cardinal_to_vector(self.cardinal))
            for c in range(self.steps)
        ]


@dataclass
class NotARoomba:
    start: Coordinates
    move_history: List[MoveCommand] = field(default_factory=list)

    def get_move_history(self) -> List[Coordinates]:
        return self.move_history

    def move_to(self, move_command: MoveCommand):
        self.move_history.append(move_command)

    def get_all_coordinates(self) -> List[Coordinates]:
        all_coordinates = [self.start]
        for move in self.move_history:
            new_moves = move.to_coordinates()
            for new_move in new_moves:
                new_coordinate = all_coordinates[-1] + new_move
                all_coordinates.append(new_coordinate)
        return all_coordinates

    def get_cleaned_places(self) -> List[Coordinates]:
        all_coordinates = self.get_all_coordinates()
        return list(set(all_coordinates))
