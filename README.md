# Not a Roomba

## Description
### Background
When you have a lot of people working in an office it can get dirty really quickly if you're not careful.
However, cleaning stuff are expensive. To save money on the cleaning staff the best solution was deemed to
be the creation of an automatic cleaning robot that cleans the office at night.

### Assignment
Your assignment is to built a prototype of that robot. The assignemt is assigned to be as simple as possible.
The robot will, once given instructions (shown below as input), run on its own without any human interference.
In the morning we can ask the robot how many unique places it has cleaned.

### Input and Output Criteria
- All input will be given on standard in.
- All output is expected on standard out.
- First input line: a single integer that expects a number of commands the roboter should expect to execute before it knows it's done.
  The number will be in the range `n` (0 <= n <= 10,000).
- Second input line: consists of two integer numbers that represent the starting coordinates `x` and `y` of the robot.
  The value of each coordinate will be in the range  of `x` (-100,000 <= 100,000).
- The third, and any subsequent line, will consist of two pieces of data.
  The first will be a single uppercase character `e` {E, W, N, S}, that represents the direction on the compass the robot should head.
  The second will be an integer representing the number of steps s (0 < s < 100,000) that the robot should take in said direction.

### Special Notes
- The robot will never be sent outside the bounds of the plane.
- All input should be considered well formed and syntactically correct. There is no need, therefore, to implement elaborate input parsing.
- Do not output any error messages. See previous point. The only output should be the number of unique places that the robot cleaned. See below.
- There will be no leading trailing white space on any line of input.
- There should be no leading or trailing white space on any line of output.
- Any mulit-valued line of input will have a single whitespace character between each value.
- You can assume, for the sake of simplicity, that the office can be viewed as a grid where the robot only moves only on the vertices.
- The robot cleans at every vertex it touches not just where it stops.



## How to run
This project uses python 3.8 but other version might be supported as well. Installation instructions are listed in the `pyproject.toml` file.
The dev version is using `pytest`, `black`, and `flake8` for code formatting. I used `poetry` for packaging.
The instructions assume that the input will be standard in/out. So I used only the `input()` function that is nativlely supported by Python.

To run the roomba just use
```
python cli.py
```

To run the tests just use the following command in the root directory
```
pytest -x
```
