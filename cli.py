import time

from brains import NotARoomba, MoveCommand, Coordinates


def parse():
    print(
        "NotARoomba: Hello, I am NotARoomba how many cleaning"
        " instructions do you have for me today?"
    )
    count = int(input("You: "))
    print("NotARoomba: Where should I start my journey? *buzzing sounds*")
    print(
        "NotARoomba: Forgive me my human language parsing skills.\n"
        "NotARoomba: I only understand the following Schema => '<x> <y>'\n"
        "NotARoomba: e.g. '1 2' or '-10 4'"
    )
    start_x, start_y = map(int, input(("You: ")).split(" "))
    move_data = []
    print(
        "NotARoomba: Ok! Please give me instructions for each command!\n"
        "NotARoomba: I only understand the following Schema => '<cardinal> <number>'\n"
        "NotARoomba: e.g. 'E 1' or 'W 10'"
    )
    for i in range(count):
        input_data = input(f"Command ({i+1}/{count}): ").split(" ")
        move_data.append({"cardinal": input_data[0], "steps": int(input_data[1])})

    not_a_roomba = NotARoomba(start=Coordinates(start_x, start_y))

    for move_vector in move_data:
        not_a_roomba.move_to(MoveCommand(move_vector["cardinal"], move_vector["steps"]))

    print(
        "NotARoomba: Thank you for choosing NotARoomba as your cleaning Staff.\n"
        "I will start cleaning right away! See you tomorrow.\n"
    )

    print("*starts cleaning*\n")

    for i in range(3):
        print(".")
        time.sleep(1)
    print()

    print("*buzzing sounds*\n")

    for i in range(6):
        print(".")
        time.sleep(1)
    print()

    print("*more buzzing sounds*\n")

    time.sleep(3)

    print(
        "NotARoomba: Good Morning! Everything is nice and shiny again."
        "Here is the cleaning report."
    )
    report = f"""
    NotARoomba Report
    -----------------
    Move Commands in total: {len(not_a_roomba.move_history)}
    Coordinates in total: {len(not_a_roomba.get_all_coordinates())}
    Number of cleaned places: {len(not_a_roomba.get_cleaned_places())}
    """
    print(report)


if __name__ == "__main__":
    parse()
